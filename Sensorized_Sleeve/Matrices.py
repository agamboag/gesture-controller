import math

class Matrix:
    # Initialization function
    def __init__(self, *args):
        self.mat = []
        if len(args) == 2:
            if type(args[0]) is int and (type(args[0]) == type(args[1]) or type(args[1]) is str):
                self.rows = args[0]
                self.cols = args[1] if type(args[1]) is int else args[0]

                for i in range(self.rows):
                    self.mat.append([0.0] * self.cols)

                if args[1] == 'i':
                    for i in range(self.rows):
                        self.mat[i][i] = 1
        
        elif len(args) == 1 and all(isinstance(x, list) for x in args[0]):
            self.rows = len(args[0])
            self.cols = len(args[0][0])
            for i in range(self.rows):
                self.mat.append([])
                for j in range(self.cols):
                    self.mat[i].append(args[0][i][j])

        elif len(args) == 1 and all(isinstance(x, (int, float, complex)) for x in args[0]):
            self.mat.append([])
            self.rows = 1
            self.cols = len(args[0])
            self.mat[0] = [x for x in args[0]]
        
        else:
            raise TypeError('Matrix parameters must be either a single array of numbers, two integers for dimensions, or an integer and \'i\'')
    
    # Return value at index
    def __getitem__(self, index):
        return self.mat[index[0]][index[1]]

    # Return 2D array and size as tuple
    def get(self):
        return [self.mat, self.rows, self.cols]
    
    # Set value at index
    def __setitem__(self, index, val):
        self.mat[index[0]][index[1]] = val
    
    # Scalar or Matrix addition
    def __add__(self, other):
        mat = Matrix(self.rows, self.cols)
        if type(other) != Matrix:
            for i in range(self.rows):
                for j in range(self.cols):
                    mat[i, j] = self[i, j] + other
        else:
            if self.dim() != other.dim():
                raise("DIMENSION ERROR: Cannot add matrices with different number of rows and columns")
            for i in range(self.rows):
                for j in range(self.cols):
                    mat[i, j] = self[i, j] + other[i, j]
        return mat
    
    def __radd__(self, other):
        return self + other
    
    # Scalar or Matrix subtraction
    def __sub__(self, other):
        mat = Matrix(self.rows, self.cols)
        if type(other) != Matrix:
            for i in range(self.rows):
                for j in range(self.cols):
                    mat[i, j] = self[i, j] - other
        else:
            if self.dim() != other.dim():
                raise CustomError("DIMENSION ERROR: Cannot Subtract matrices with different number of rows and columns")
            
            for i in range(self.rows):
                for j in range(self.cols):
                    mat[i, j] = self[i, j] - other[i, j]
        return mat
    
    def __rsub__(self, other):
        return (-self) + other
    
    def __neg__(self):
        mat = Matrix(self.rows, self.cols)
        for i in range(self.rows):
            for j in range(self.cols):
                mat[i, j] = self[i, j] * -1
        return mat

    # Scalar or Matrix multiplication
    def __mul__(self, other):
        mat = Matrix(self.rows, self.cols)
        if type(other) != Matrix:
            for i in range(self.rows):
                for j in range(self.cols):
                    mat[i, j] = self[i, j] * other
        else:
            if self.dim() != other.dim():
                raise CustomError('DIMENSION ERROR: Cannot Multiply matrices with different number of rows and columns')
            
            for i in range(self.rows):
                for j in range(self.cols):
                    mat[i, j] = self[i, j] * other[i, j]
        
        return mat
    
    def __rmul__(self, other):
        return self * other

    def __truediv__(self, other):
        mat = Matrix(self.rows, self.cols)
        if type(other) != Matrix:
            for i in range(self.rows):
                for j in range(self.cols):
                    mat[i, j] = self[i, j] / other
        else:
            if self.dim() != other.dim():
                raise CustomError('DIMENSION ERROR: Cannot Divide matrices with different number of rows and columns')
            
            for i in range(self.rows):
                for j in range(self.cols):
                    mat[i, j] = self[i, j] / other[i, j]
        
        return mat

    def __matmul__(self, other):
        if type(other) != Matrix:
            raise TypeError('Cannot matrix multiply Matrix by ', type(other))
        mat = Matrix(self.rows, other.dim()[1])
        if self.cols != other.dim()[0]:
            raise CustomError("DIMENSION ERROR: Cannot Matrix Multiply matrices with mismatched number of rows and columns")
        
        for i in range(self.rows):
            for j in range(other.dim()[1]):
                for k in range(self.cols):
                    mat[i, j] += self[i, k] * other[k, j]
        return mat
    
    # Raise the Matrix to a power
    def __pow__(self, power):
        if self.rows != self.cols:
            raise CustomError('DIMENSION ERROR: Cannot raise a non square Matrix to a power.')
        
        if power < 0 or int(power) != power:
            raise CustomError('EXPONENT ERROR: Cannot raise a Matrix to a negative or floating point exponent')

        if power == 0:
            return Matrix(self.rows, self.cols, True)

        mat = self
        for i in range(power - 1):
            mat = mat @ self
        return mat
    
    # Take the pseudo transpose of a Matrix
    def transpose(self):
        mat = Matrix(self.cols, self.rows)
        
        for i in range(self.cols):
            for j in range(self.rows):
                mat[i, j] = self[j, i]
        return mat

    def __repr__(self):
        toPrint = ""
        for i in range(self.rows):
            for j in range(self.cols):
                toPrint += "%9.4f " % self.mat[i][j]
            toPrint += "\n"
        return toPrint
    
    def __len__(self):
        return self.cols * self.rows
    
    def dim(self):
        return (self.rows, self.cols)
    
    def norm(self):
        tot = 0
        for i in range(self.rows):
            for j in range(self.cols):
                tot += self.mat[i][j] ** 2
        return 1 / math.sqrt(tot)
    
    def __iter__(self):
        self.i = 0
        self.j = 0
        return self

    def __next__(self):
        try:
            toReturn = self[self.i, self.j]
            self.j += 1
            if(self.j >= self.cols):
                self.j = 0
                self.i += 1
            return toReturn
        except IndexError:
            raise StopIteration
    
    def inverse(self):
        if self.dim == (1,1):
            return Matrix([[1/self[0,0]]])
        if self.dim() == (2,2):
            det = self.determinant()
            return (1/det) * Matrix([[self[1,1], -self[0,1]],[-self[1,0], self[0,0]]])
        print('Matrix size is not supported for inverse yet.')
        return None

    def determinant(self):
        if self.dim() == (1,1):
            return self[0,0]
        elif self.dim() == (2,2):
            return self[0,0] * self[1,1] - self[0,1] * self[1,0]

class CustomError(Exception):
    pass
