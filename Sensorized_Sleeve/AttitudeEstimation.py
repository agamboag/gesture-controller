from Matrices import Matrix
from math import *

##################################
# @function angles2dcm(yaw, pitch, roll)
# @param yaw angle in radians along the x axis
# @param pitch angle in radians along the y axis
# @param roll angle in radians along the z axis
# @return dcm a 3x3 directional cosine matrix
# @brief Converts the Euler angles in radians 
#        to a directional cosine matrix
##################################
def angles2dcm(yaw, pitch, roll):
    dcm = Matrix([[cos(pitch) * cos(yaw), cos(pitch) * sin(yaw), -1 * sin(pitch)], # Directional cosine matrix
            [sin(roll) * sin(pitch) * cos(yaw) - cos(roll) * sin(yaw), sin(roll) * sin(pitch) * sin(yaw) + cos(roll) * cos(yaw), sin(roll) * cos(pitch)],
            [cos(roll) * sin(pitch) * cos(yaw) + sin(roll) * sin(yaw), cos(roll) * sin(pitch) * sin(yaw) - sin(roll) * cos(yaw), cos(roll) * cos(pitch)]])
    
    return dcm

##################################
# @function dcm2angles(dcm)
# @param dcm A rotation matrix
# @return An array containing the 3 Euler angles
# @brief Converts the directional cosine matrix to euler angles in radians
##################################
def dcm2angles(dcm):
    theta = asin(-dcm[0,2] if abs(dcm[0,2]) <= 1 else -dcm[0,2] / abs(dcm[0,2])) # Get theta
    
    #if pi / 2 - 0.01 > theta > - pi / 2 + 0.01: # Check if theta is 90 degrees
    if abs(theta) != pi / 2:
        psiS = dcm[0,1] # Get sin of psi
        psiC = dcm[0,0] # Get cos of psi
        
        phiS = dcm[1,2] # Get sin of phi
        phiC = dcm[2,2] # Get cos of phi
    else:
        psiS = -dcm[1,0] + dcm[0,2] * dcm[2,1] # Get sin of psi
        psiC = dcm[0,2] * dcm[2,0] + dcm[1,1] # get cos of psi

        phiS = dcm[2,0] - dcm[0,2] * dcm[1,1] # Get sin of phi
        phiC = -dcm[0,2] * dcm[1,0] + dcm[2,1] # Get cos of phi

    #add = atan((-dcm[1,0]-dcm[2,1]) / (dcm[1,1] - dcm[2,0]))
    try:
        psi = atan(psiS / psiC) # Calculate psi 
    except ZeroDivisionError:
        psi = 0
    
    try:
        phi = atan(phiS / phiC) # Calculate phi
    except ZeroDivisionError:
        phi = 0

    # Get quadrant of psi and phi
    psi = pi + psi if psiC < 0 and psiS >= 0 else -pi + psi if psiC < 0 and psiS < 0 else psi
    phi = pi + phi if phiC < 0 and phiS >= 0 else -pi + phi if phiC < 0 and phiS < 0 else phi

    return [psi, theta, phi]

##################################
# @function dcm2quat(dcm)
# @param dcm A rotation matrix
# @return A quaternion vector
# @brief Converts the directional cosine matrix to a quaternion
##################################
def dcm2quat(dcm):
    quat = None
    
    b = []
    b.append(1 + dcm[0,0] + dcm[1,1] + dcm[2,2])
    b.append(1 + dcm[0,0] - dcm[1,1] - dcm[2,2])
    b.append(1 - dcm[0,0] + dcm[1,1] - dcm[2,2])
    b.append(1 - dcm[0,0] - dcm[1,1] + dcm[2,2])
    
    i = b.index(max(b))
    
    if i == 0:
        b1 = sqrt(b[0]) / 2
        quat = Matrix([[b1],
                       [(dcm[2,1] - dcm[1,2]) / (4 * b1)],
                       [(dcm[0,2] - dcm[2,0]) / (4 * b1)],
                       [(dcm[1,0] - dcm[0,1]) / (4 * b1)]])
    elif i == 1:
        b2 = sqrt(b[1]) / 2
        quat = Matrix([[(dcm[2,1] - dcm[1,2]) / (4 * b2)],
                       [b2],
                       [(dcm[0,1] + dcm[1,0]) / (4 * b2)],
                       [(dcm[0,2] + dcm[2,0]) / (4 * b2)]])
    elif i == 2:
        b3 = sqrt(b[2]) / 2
        quat = Matrix([[(dcm[2,0] - dcm[0,2]) / (4 * b3)],
                       [(dcm[0,1] + dcm[1,0]) / (4 * b3)],
                       [b3],
                       [(dcm[1,2] + dcm[2,1]) / (4 * b3)]])
    elif i == 3:
        b4 = sqrt(b[3]) / 2
        quat = Matrix([[(dcm[1,0] - dcm[0,1]) / (4 * b4)],
                       [(dcm[0,2] + dcm[2,0]) / (4 * b4)],
                       [(dcm[1,2] + dcm[2,1]) / (4 * b4)],
                       [b4]])
    return quat

##################################
# @function rcross(r)
# @param r A 3x1 column vector
# @return a skew symmetric matrix
# @brief Creates a symmetric matrix from column vector r
##################################
def rcross(r):
    mat = Matrix([[0, -r[2,0], r[1,0]], 
                  [r[2,0], 0, -r[0,0]], 
                  [-r[1,0], r[0,0], 0]])
    
    return mat

##################################
# @function Rexp(w, deltaT)
# @param w A 3x1 column vector of angular velocity in rads/s
# @param deltaT The time interval between measurements in s
# @return A rotation matrix
# @brief Takes the r exponential function of angular velocity
##################################
def Rexp(w, deltaT):
    wnorm = sqrt(w[0,0] ** 2 + w[1,0] ** 2 + w[2,0] ** 2)
    wx = rcross(w)
    
    if wnorm < 0.2: # Small angle taylor expansion
        sincW = deltaT - ((deltaT ** 3) * (wnorm ** 2)) / 6 + ((deltaT ** 5) * (wnorm ** 4)) / 120
        oneMinusCosW = (deltaT ** 2) / 2.0 - ((deltaT ** 4) * (wnorm ** 2)) / 24.0 + ((deltaT ** 6) * (wnorm ** 4)) / 720.0
    else:
        sincW = sin(wnorm * deltaT) / wnorm
        oneMinusCosW = (1 - cos(wnorm * deltaT)) / (wnorm ** 2)
    
    return Matrix(3, 'i') - (wx * sincW) + ((wx ** 2) * oneMinusCosW)

##################################
# @function IntegrateOpenLoop(Rminus, gyros, deltaT)
# @param Rminus A directional cosine matrix
# @param gyros A 3x1 column vector of angular velocity
# @param deltaT Time interval between measurements
# @return Updated DCM
##################################
def IntegrateOpenLoop(Rminus, gyros, deltaT):
    rexp = Rexp(gyros, deltaT)
    return rexp @ Rminus

##################################
# @function IntegrateClosedLoop(Rminus, Bminus, accels, mags, w, accelInertial, magInertial, deltaT)
# @param Rminus A directional cosine matrix
# @param Bminus Gyroscope bias column vector
# @param accels Normalized accelerometer readings
# @param mag Normalized magnetometer readings
# @param w Gyroscope readings in rads/s
# @param accelInertial Inertial accelerometer frame
# @param magInertial Inertial magnetometer frame
# @param deltaT time interval between measurements
# @return An updated DCM and bias vector
# @brief performs closed loop integration on a rotation matrix using accelerometer
#        magnetometer, and gyro readings and takes into the bias
##################################
def IntegrateClosedLoop(Rminus, Bminus, accels, mags, w, accelInertial, magInertial, deltaT):
    KP_A = 10
    KI_A = KP_A / 10.0
    KP_M = 10
    KI_M = KP_M / 10.0

    accelN = accels * accels.norm()
    magsN = mags * mags.norm()
    accelInertialN = accelInertial * accelInertial.norm()
    magInertialN = magInertial * magInertial.norm()

    gyroInputWithBias = w - Bminus

    wmeas_a = rcross(accelN) @ (Rminus @ accelInertialN)
    wmeas_m = rcross(magsN) @ (Rminus @ magInertialN)

    wtot = gyroInputWithBias + (KP_A * wmeas_a) + (KP_M * wmeas_m)

    Rplus = IntegrateOpenLoop(Rminus, wtot, deltaT)

    bdot = -KI_A * wmeas_a - KI_M * wmeas_m
    Bplus = Bminus + bdot * deltaT

    return [Rplus, Bplus]











