import qwiic_icm20948 as ICM
import signal
from Matrices import Matrix
from AttitudeEstimation import *
import os
import math

### DEFINE macros ###
d2r = lambda deg : deg * math.pi / 180
r2d = lambda rad : rad * 180 / math.pi

### Global Variables ###
flag = False

def main():
    global flag # Get global bool
    
    dT = 1/50 # Interval between data reads
    a0 = Matrix(3, 1) # Accelerometer col vector
    m0 = Matrix(3, 1) # Magnetometer col vector
    aI0 = Matrix(3, 1) # Accelerometer col Inertial vector
    mI0 = Matrix(3, 1) # Magnetometer col Inertial vector
    w0 = Matrix(3, 1) # Magnetometer col vector
    a1 = Matrix(3, 1) # Accelerometer col vector
    m1 = Matrix(3, 1) # Magnetometer col vector
    aI1 = Matrix(3, 1) # Accelerometer col Inertial vector
    mI1 = Matrix(3, 1) # Magnetometer col Inertial vector
    w1 = Matrix(3, 1) # Magnetometer col vector
    filtered = Matrix([[1], [0], [0]]) # Initial position

    # Magnetometer 0 correction values
    M0s = Matrix([[1.4869, -0.0017, 0.0077],
                  [-0.0029, 1.4317, 0.0015],
                  [0.0040, 0.0019, 1.4212]])
    M0b = Matrix([[0.8999],[0.2451],[-0.8444]])

    # Accelerometer 0 correction values
    A0s = Matrix([[1.0104, -0.0040, 0.0143],
                  [-0.0040, 0.9895, 0.0001],
                  [0.0142, 0.0000, 0.9865]])
    A0b = Matrix([[-0.0899],[0.0413],[0.0556]])
    
    # Magnetometer 1 correction values
    M1s = Matrix([[1.5241, -0.0048, 0.0067],
                  [-0.0069, 1.4437, -0.0003],
                  [0.0031, -0.0000, 1.4574]])
    M1b = Matrix([[0.7447], [0.4502], [0.4850]])
    
    # Accelerometer 1 correction values
    A1s = Matrix([[1.0081, -0.0026, 0.0163],
                  [-0.0028, 0.9896, 0.0022],
                  [0.0158, 0.0025, 0.9903]])
    A1b = Matrix([[-0.0880], [-0.0396], [0.0267]])
    
    # Magnetometer 0 misalignment matrix
    Rmis0 = Matrix([[0.9999, -0.0069, -0.0068],
                    [0.0068, 0.9999, -0.0131],
                    [0.0069, 0.0130, 0.9999]])
    
    # Magnetometer 1 misalignment matrix
    Rmis1 = Matrix([[0.9999, -0.0090, -0.0028],
                    [0.0090, 0.9999, -0.0131],
                    [0.0030, 0.0131, 0.9999]])
    
    R0 = Matrix(3, 'i') # DCM 0
    B0 = Matrix(3, 1) # Bias vector 0
    
    R1 = Matrix(3, 'i') # DCM 1
    B1 = Matrix(3, 1) # Bias vector 1
    
    neg = Matrix([[1, 0, 0], [0, -1, 0], [0, 0, -1]]) # Negative of y and z axes to align magnetometer and accelerometer
    y2z = Matrix([[1, 0, 0], [0, 0, 1], [0, 1, 0]]) # 90 degree x rotation matrix
    
    try:
        f = open('data.txt', 'x', os.O_NONBLOCK)
    except FileExistsError:
        f = open('data.txt', 'w', os.O_NONBLOCK)
    try:
        log = open('vertical2.csv', 'x', os.O_NONBLOCK)
    except FileExistsError:
        log = open('vertical2.csv', 'w', os.O_NONBLOCK)
        log.truncate(0)

    signal.signal(signal.SIGALRM, timerISR) # Set Handler function for Timer ISR
    signal.setitimer(signal.ITIMER_REAL, dT, dT) # Set interval for Timer ISR
    
    # Initialize IMUs
    IMU0 = ICM_Init(addr=0x69)
    IMU1 = ICM_Init(addr=0x68)
    if IMU0 == None or IMU1 == None:
        print('IMU Not connected')
        exit()
    
    i = 0
    while i < 150:
        if flag: # Check if dT time has elapsed
            flag = False # Toggle flag
            
            try:
                while not IMU0.dataReady(): # Wait until data is ready to be read
                    pass
                while not IMU1.dataReady(): # Wait until data is ready to be read
                    pass

                IMU0.getAgmt() # Read all sensors
                IMU1.getAgmt() # Read all sensors
                
                # Scale and Normalize accel data
                a0[0,0] = IMU0.axRaw / 16384
                a0[1,0] = IMU0.ayRaw / 16384
                a0[2,0] = IMU0.azRaw / 16384
                a0 = A0s @ a0 + A0b
                
                # # Scale and Normalize mag data
                m0[0,0] = IMU0.mxRaw * 0.15 / 47.428
                m0[1,0] = IMU0.myRaw * 0.15 / 47.428
                m0[2,0] = IMU0.mzRaw * 0.15 / 47.428
                m0 = Rmis0 @ (neg @ (M0s @ m0 + M0b))

                # Scale and Normalize accel data
                a1[0,0] = IMU1.axRaw / 16384
                a1[1,0] = IMU1.ayRaw / 16384
                a1[2,0] = IMU1.azRaw / 16384
                a1 = A1s @ a1 + A1b
                
                # Scale and Normalize mag data
                m1[0,0] = IMU1.mxRaw * 0.15 / 47.428
                m1[1,0] = IMU1.myRaw * 0.15 / 47.428
                m1[2,0] = IMU1.mzRaw * 0.15 / 47.428
                m1 = Rmis1 @ (neg @ (M1s @ m1 + M1b))
                
                aI0 = aI0 + a0
                mI0 = mI0 + m0
                aI1 = aI1 + a1
                mI1 = mI1 + m1
                
                i+= 1
            except OSError:
                print('I2C connection lost!')
                exit()
    
    # Calculate Inertial reference frame
    # NOTE: This assumes that x axes are aligned and horizontal
    aI0 = aI0 * aI0.norm()
    mI0 = mI0 * mI0.norm()
    aI1 = aI1 * aI1.norm()
    mI1 = mI1 * mI1.norm()
    
    print(aI0, mI0, aI1, mI1)
    toPrint = ', '.join(['{:.4f}'.format(x) for x in aI0] + ['{:.4f}'.format(x) for x in mI0] + 
                        ['{:.4f}'.format(x) for x in [0, 0, 0]] + ['{:.4f}'.format(x) for x in [0, 0, 0]] +
                        ['{:.4f}'.format(x) for x in aI1] + ['{:.4f}'.format(x) for x in mI1] + 
                        ['{:.4f}'.format(x) for x in [0, 0, 0]] + ['{:.4f}'.format(x) for x in [0, 0, 0]])
    log.write(toPrint + '\n')
    
    ### Main Loop ###
    while True:
        if flag: # Check if dT time has elapsed
            flag = False # Toggle flag
            
            try:
                while not IMU0.dataReady(): # Wait until data is ready to be read
                    pass
                while not IMU1.dataReady(): # Wait until data is ready to be read
                    pass
            
                IMU0.getAgmt() # Read all sensors
                IMU1.getAgmt() # Read all sensors
                
                # Scale and Normalize accel data
                a0[0,0] = IMU0.axRaw / 16384
                a0[1,0] = IMU0.ayRaw / 16384
                a0[2,0] = IMU0.azRaw / 16384
                a0 = A0s @ a0 + A0b
                
                # # Scale and Normalize mag data
                m0[0,0] = IMU0.mxRaw * 0.15 / 47.428
                m0[1,0] = IMU0.myRaw * 0.15 / 47.428
                m0[2,0] = IMU0.mzRaw * 0.15 / 47.428
                m0 = Rmis0 @ (neg @ (M0s @ m0 + M0b))
                
                # Scale and reduce gyro drift noise
                w0[0,0] = (IMU0.gxRaw - 352) / 131
                w0[1,0] = (IMU0.gyRaw - 127) / 131
                w0[2,0] = (IMU0.gzRaw - 9) / 131

                # Scale and Normalize accel data
                a1[0,0] = IMU1.axRaw / 16384
                a1[1,0] = IMU1.ayRaw / 16384
                a1[2,0] = IMU1.azRaw / 16384
                a1 = A1s @ a1 + A1b
                
                # Scale and Normalize mag data
                m1[0,0] = IMU1.mxRaw * 0.15 / 47.428
                m1[1,0] = -IMU1.myRaw * 0.15 / 47.428
                m1[2,0] = -IMU1.mzRaw * 0.15 / 47.428
                m1 = Rmis1 @ (neg @ (M1s @ m1 + M1b))

                # Scale and reduce gyro drift noise
                w1[0,0] = (IMU1.gxRaw + 70) / 131
                w1[1,0] = (IMU1.gyRaw - 28) / 131
                w1[2,0] = (IMU1.gzRaw - 60) / 131
                
                R0, B0 = IntegrateClosedLoop(R0, B0, a0, -m0, w0 * d2r(1), aI0, -mI0, dT)
                R1, B1 = IntegrateClosedLoop(R1, B1, a1, -m1, w1 * d2r(1), aI1, -mI1, dT)
                
                ep0 = R0 @ Matrix([[0.5], [0], [0]])
                ep1 = y2z @ (R1 @ Matrix([[0.5], [0], [0]])) # Swap Y and Z axes since the gyro is rotated 90 deg realive to coordinate frame
                filtered = 0.8 * filtered + (1 - 0.8) * (ep0 + ep1) # IIR Filter to smooth endpoint
                toPrint = ', '.join(['{:.4f}'.format(x) for x in a0] + ['{:.4f}'.format(x) for x in m0] + 
                                  ['{:.4f}'.format(x) for x in w0] + ['{:.4f}'.format(x) for x in ep0] +
                                  ['{:.4f}'.format(x) for x in a1] + ['{:.4f}'.format(x) for x in m1] + 
                                  ['{:.4f}'.format(x) for x in w1] + ['{:.4f}'.format(x) for x in ep1])
                log.write(toPrint + '\n')

                toPrint = ', '.join(['{:.2f}'.format(x) for x in filtered]) # Only export two decimal points to reduce noise 
                f.truncate(0)
                f.write(toPrint)
                f.flush()
                print(toPrint)
                
            except OSError:
                print('I2C connection lost!')
                exit()


# Timer Interrupt Routine
def timerISR(sigNum, stack_frame):
    global flag
    flag = True

# ICM Initialization function
def ICM_Init(addr=0x69):
    try:
        IMU = ICM.QwiicIcm20948(address=addr) # Initialize I2C

        if IMU.connected == False: # Check if successfully connected
            return None

        IMU.begin() # Begin reading data
        return IMU
    except OSError:
        return None

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Keyboard Interrupt. Terminating program.')
        exit()