#! /usr/bin/env python

import rospy
import intera_interface
from intera_interface import CHECK_VERSION
import signal
import time
from std_msgs.msg import UInt16
import zmq
from intera_motion_interface import (
    MotionTrajectory,
    MotionWaypoint,
    MotionWaypointOptions
)
from intera_motion_msgs.msg import TrajectoryOptions
from geometry_msgs.msg import PoseStamped
import math
import ik_service_client_v2
import os

limb = None

def main():
    global limb
    # ROS Node Initialization

    number = 1

    f = open('/home/flegal/Desktop/endpoint6.csv', 'w', os.O_NONBLOCK)
    f.truncate(0)
    rospy.init_node('follow_endpoint_py')
    print('Node Initialized')
    pub_rate = rospy.Publisher('robot/joint_state_publish_rate', UInt16, queue_size=10)

    # Open connection to the Pi
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect('tcp://10.0.0.10:5555')
    print('Connected to Raspberry Pi')

    socket.send(b'CONNECT CHECK')
    message = socket.recv_string()

    # Initialize the limb
    limb = intera_interface.limb.Limb('right')
    names = limb.joint_names()
    rs = intera_interface.RobotEnable(CHECK_VERSION)
    rs.enable()
    pub_rate.publish(20)

    def reset_control_modes():
        rate = rospy.Rate(20)
        for i in range(100):
            if rospy.is_shutdown():
                return False
            limb.exit_control_mode()
            pub_rate.publish(100)
            rate.sleep()

    def shutdown():
        f.close()
        print('Shutting down')
        reset_control_modes()
        limb.move_to_neutral(timeout=5)
    rospy.on_shutdown(shutdown)

    # Move to initial position (fixes possible issue of being stuck)
    limb.move_to_neutral(timeout=5.0)

    # Initialize ROS clock
    rate = rospy.Rate(20)

    # Initialize Feedback loop
    control = Feedback(limb)
    vel = control.getVels()
    pos = [x for x in limb.joint_angles().values()]
    init = pos[-number - 1]
    pos[-number - 1] = pos[-number - 1]

    control.setAngles(pos)

    while not rospy.is_shutdown():
        pub_rate.publish(50)
        # Get endpoint data
        socket.send(b'REQUEST DATA')
        message = socket.recv_string().encode().replace('\x00', '').strip()
        print(message)
        # Normalize endpoint
        if len(message) < 3:
            limb.set_joint_velocities(vel)
            rate.sleep()
            continue
        try:
            endpoints = [float(i) for i in message.split(', ')]
        except ValueError:
            print('Non float value in string: %s', message)
        if len(endpoints) < 3:
            rospy.logerr('Did not receive enough angle arguments')
        x, y, z = vectorNorm(endpoints[0], endpoints[1], endpoints[2])

        f.write('{:0.2f}, {:0.2f}, {:0.2f}, '.format(x, y, z))
        
        angles = ik_service_client_v2.ik_service_client([x, -y, -z], [1, 0, 0, 0])
        
        try:
            control.setAngles([angles[len(angles) - 1 - i] for i in range(len(angles))])
            control.setAngles(angles[::-1])
        except:
            continue

        control.feedback()
        vel = control.getVels()
        limb.set_joint_velocities(vel)
        # f.write('{:.4f}\r\n'.format(limb.joint_angles().values()[-number - 1] - init))
        pose = limb.endpoint_pose()
        f.write(str([[i for i in pose[y]] for x, y in enumerate(pose)]))
        #f.write(', ')
        #f.write(str([[i for i in pose[y]] for x, y in enumerate(limb.endpoint_pose())]))
        f.write('\r\n')
        rate.sleep()

class Feedback:
    def __init__(self, limb):
        self.a1 = [0]*7
        self.x1 = [0]*7
        self.u1 = [0]*7
        self.dT = 0.02
        self.limb = limb
        names = limb.joint_names()
        self.names = [names[len(names) - 1 - i] for i in range(len(names))]
        self.des_angles = [x for x in self.limb.joint_angles().values()]
    
    def setAngles(self, angles):
        self.des_angles = angles

    def feedback(self):
        cur_angles = [x for x in self.limb.joint_angles().values()]
        for i in range(len(cur_angles)):
            e0 = self.des_angles[i] - cur_angles[i]
            u_p = 1.4124 * e0
            a0 = self.a1[i] + e0 * self.dT
            u_i = 0.3853 * a0
            u0 = u_p + u_i
            if abs(u0) > 0.5:
                a0 = self.a1[i]
                u_i = 0
                u0 = 0.5 if u0 > 0 else -0.5
            self.u1[i] = u0
            self.x1[i] = cur_angles[i]
            self.a1[i] = a0
    
    def getVels(self):
        vels =  {self.names[i]:self.u1[i] for i in range(len(self.names))}
        return vels

def vectorNorm(x, y, z):
    norm = math.sqrt(x**2 + y**2 + z**2)
    if norm > 0.95:
        norm = math.sqrt(x**2 + y**2 + z**2) + 0.1
        return [x / norm, y / norm, z / norm]
    else:
        return [x, y, z]

if __name__=='__main__':
    main()